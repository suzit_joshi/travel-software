                            <!--EXPENSE FORM-->
                            <div id="expense-form">
                                <input type="hidden" name="cash_id" value="">
                                <input type="hidden" name="current_cash" value="" id="current-cash">
                                <div class="form-group">
                                    <label style="display:block;">Payment Type</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="expense_payment_type" class="expense-payment-type" value="Cash" checked> Cash
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="expense_payment_type" class="expense-payment-type" value="Cheque"> Cheque
                                    </label>
                                </div>

                                <div class="form-group" id="cheque-no">
                                    <div class="row">
                                        <label class="col-md-2">Cheque No</label>
                                        <div class="col-md-4">
                                            <input type="text" name="cheque_no" class="form-control">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>Expense Category</label>
                                    <select class="form-control" name="expense_category" id="expense-category">
                                        <option value="0">Select One</option>
                                        <option value="candidate">Candidate</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Candidate</label>
                                    <select class="form-control" name="expense_candidate" id="expense-candidate">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Expense For</label>
                                    <select class="form-control" name="expense_for" id="expense-for">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                            </div>
                            <!--End Expense Form-->