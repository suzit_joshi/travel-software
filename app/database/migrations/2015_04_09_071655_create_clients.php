<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClients extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	   Schema::create('clients',function($newtable){
           $newtable->increments('id');
           $newtable->string('client_name',150);
           $newtable->string('address',100);
           $newtable->string('phone',20);
           $newtable->string('email_id')->unique();
           $newtable->string('passport')->unique();
           $newtable->timestamps();           
       });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
