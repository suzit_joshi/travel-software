<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirlines extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	   Schema::create('airlines',function($newtable){
           $newtable->increments('id');
           $newtable->string('airlines',150)->unique();
           $newtable->string('address',100);
           $newtable->string('phone',20);
           $newtable->string('email_id')->unique();
           $newtable->timestamps();           
       });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('airlines');
	}

}
