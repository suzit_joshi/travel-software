<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('transactions',function($newtable){
           $newtable->increments('id');
           $newtable->string('transaction_no')->unique();
           $newtable->dateTime('transaction_date');
           $newtable->string('type',20);
           $newtable->string('amount');
           $newtable->string('payment_type');
           $newtable->string('voucher_no',50);
           $newtable->string('cheque_no',50);
           $newtable->text('remarks');           
            
           $newtable->integer('client_id')->unsigned();
           
           $newtable->integer('category_id')->unsigned();
            
           $newtable->integer('ticket_id')->unsigned();

           $newtable->timestamps();           
       });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
