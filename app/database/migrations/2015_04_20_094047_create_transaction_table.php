<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('transactions',function($newtable){
           $newtable->increments('id');
           $newtable->string('transaction_no')->unique();
           $newtable->dateTime('transaction_date');
           $newtable->string('type',20);
           $newtable->string('amount');
           $newtable->string('payment_type');
           $newtable->string('voucher_no',50);
           $newtable->string('cheque_no',50);
           $newtable->text('remarks');           
            
           $newtable->integer('clients_id')->unsigned();
           $newtable->foreign('clients_id')->references('id')->on('clients');
           
           $newtable->integer('category_id')->unsigned();
           $newtable->foreign('category_id')->references('id')->on('categories');
            
           $newtable->integer('ticket_id')->unsigned();
           $newtable->foreign('ticket_id')->references('id')->on('tickets');

           $newtable->timestamps();           
       });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
