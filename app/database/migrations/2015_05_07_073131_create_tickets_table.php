<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
	{
        Schema::create('tickets',function($newtable){
           $newtable->increments('id');
           $newtable->string('ticket_no',100)->unique();
           $newtable->string('departure',255);
           $newtable->string('destination',255);
           $newtable->integer('client_id')->unsigned();
           $newtable->integer('airline_id')->unsigned();
           $newtable->float('total_fare');
           $newtable->dateTime('departure_time');
           $newtable->timestamps();           
       });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
