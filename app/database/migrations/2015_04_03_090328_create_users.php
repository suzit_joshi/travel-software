<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users',function($newtable){
           $newtable->increments('id');
           $newtable->string('username',100)->unique();
           $newtable->string('password',255);
           $newtable->string('email_id')->unique();
            $newtable->string('remember_token',100);
           $newtable->timestamps();           
       });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
