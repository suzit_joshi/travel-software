<?php
class CategoryController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Show list of clients
     */
    public function index()
    {
        $data['income_category'] = Category::where('type', 'income')->get();
        $data['expense_category'] = Category::where('type', 'expense')->get();
        $this->layout->content = View::make('admin.transaction_category.index',$data);
    }
    

    /**
     * Save transaction category
     */
    public function saveCategory()
    {
        $id = Input::get('id');
        $validator = Validator::make(Input::all(),
                                    array(
                                        'title' => 'required|unique:categories,category_title', 
                                    )
                        );
        if($validator->fails()){
            return Redirect::route('category')->withErrors($validator->messages());       
        }
        else{
            $category = new Category();
            if($id == ""){
                $msg = 'New category created successfully !';
                
            }else{
                /*$airline = Airline::find($id);*/
                $msg = 'Category details updated successfully !';
            }
            
            $category->type = Input::get('type');
            $category->subtype = Input::get('sub_type');
            $category->category_title = Input::get('title');
            $category->save();
            
            Session::flash('success-message', $msg);
            return Redirect::route('category');
        }
    }
    
    

}

/*END OF client CONTROLLER*/
