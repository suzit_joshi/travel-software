<?php
class TicketController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Show list of clients
     */
    public function index()
    {
        $data['tickets'] = Ticket::with('airline','client')->get();
        $this->layout->content = View::make('admin.ticket.index',$data);
    }
    
    /**
     * Return form to create new clients
     */
    public function getNewForm()
    {
        $tmp_client = Client::all();
        $clients = array('' => 'Select One');
        foreach ($tmp_client as $client) {
            $tmp_client_name = $client->client_name."-".$client->passport;
            //$tmp = array($client->id => $tmp_client_name);
            $clients[$client->id] = $tmp_client_name;
            
           /* echo $client->id;
            array_push($clients, $client->id=$client->client_name."-".$client->passport);*/
        }
        $data['clients'] = $clients;
        
        $tmp_airline = Airline::all();
        $airlines = array('' => 'Select One');
        foreach ($tmp_airline as $airline) {
            $airlines[$airline->id] = $airline->airlines;
        }
        $data['airlines'] = $airlines;
        $this->layout->content = View::make('admin.ticket.newticket', $data);
    }
    
    
     /**
     * Load the view for specific clients for edit purpose
     */
    public function getRecord($id)
    {
        return $view= View::make('admin.clients.newclients')->with('client', client::find($id));
    }
    
    /**
     * Save the ticket
     */
    public function saveRecord()
    {
        $validator = Validator::make(Input::all(),
                                    array(
                                        'client' => 'required',
                                        'airlines' => 'required',
                                        'ticket_no' => 'required|unique:tickets,ticket_no',
                                        'departure' => 'required',
                                        'destination' => 'required',
                                        'charge' => 'numeric|required',
                                        'advance' => 'numeric'
                                    )
                        );
        if($validator->fails()){
                return Redirect::route('new-ticket')->withErrors($validator->messages())->withInput();
        }
        else{
            $ticket = new Ticket();
            $adv_amount = Input::get('advance');
            
            $ticket->client_id = Input::get('client');

            $ticket->airline_id = Input::get('airlines');
            $ticket->ticket_no = Input::get('ticket_no');
            $ticket->departure = Input::get('departure');
            $ticket->destination = Input::get('destination');
            $ticket->total_fare = Input::get('charge');
            
            $ticket->save();
            $ticket_id = $ticket->id;
            
            /*Transaction Related Fields*/
            if($adv_amount != "" && $adv_amount != 0 ){
                $transaction = new Transaction();
                $transaction->transaction_no = "inc_".str_random(20);
                $transaction->transaction_date = date('Y-m-d');
                $transaction->type = "income";
                $transaction->amount = $adv_amount;
                $transaction->payment_type = "Cash";
                $transaction->client_id = $ticket->client_id;
                $transaction->category_id = "1";
                $transaction->ticket_id = $ticket_id;
                $transaction->save();  
            }
            
            $msg = "Ticket Details inserted successfully!";
            Session::flash('success-message', $msg);
            return Redirect::route('list-tickets');
        }
    }
    
}

/*END OF client CONTROLLER*/
