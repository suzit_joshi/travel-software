<?php
class ClientController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Show list of clients
     */
    public function index()
    {
        $data['clients'] = Client::all();
        $this->layout->content = View::make('admin.client.index',$data);
    }
    
    /**
     * Return form to create new clients
     */
    public function getNewForm()
    {
        $this->layout->content = View::make('admin.client.newclient');
    }
    
    /**
     * Save the contents of clients
     */
    public function saveRecord()
    {
        $id = Input::get('id');
        $validator = Validator::make(Input::all(),
                                    array(
                                        'client' => 'required|max:150 ',
                                        'address' => 'required|max:100',
                                        'email' => 'email|unique:clients,email_id',
                                        'passport' => 'required',
                                        'username' => 'required|unique:users,username',
                                        'password' => 'required',
                                        'password_again' => 'required|same:password'
                                    )
                        );
        
        if($validator->fails()){
            if($id == ""){
                return Redirect::route('new-client')->withErrors($validator->messages())->withInput();
            }else{
                return Redirect::route('edit-client',$id)->withErrors($validator->messages())->withInput();
            }
        }
        else{
            $client = new Client();
            if($id == ""){
                $msg = 'New client registered successfully !';
                
            }else{
                $client = client::find($id);
                $msg = 'Client details updated successfully !';
            }
            
            $client->client_name = Input::get('client');
            $client->address = Input::get('address');
            $client->phone = Input::get('phone');
            $client->email_id = Input::get('email');
            $client->passport = Input::get('passport');
            $client->save();
            $client_id = $client->id;
            
            $user = new User();
            $user->username = Input::get('username');
            $user->password = Hash::make(Input::get('password'));
            $user->client_id = $client_id;
            $user->save();
            
            
            Session::flash('success-message', $msg);
            return Redirect::route('list-clients');
        }
       
    }
    
     /**
     * Load the view for specific clients for edit purpose
     */
    public function getRecord($id){
       return $view= View::make('admin.clients.newclients')->with('client', client::find($id));
    }
    
}

/*END OF client CONTROLLER*/
