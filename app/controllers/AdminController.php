<?php
class AdminController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Show the user profile.
     */
    public function index()
    {
        $this->layout->content = View::make('admin.index');
    }

}
?>