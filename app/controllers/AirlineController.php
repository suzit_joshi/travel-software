<?php
class AirlineController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Show list of airlines
     */
    public function index()
    {
        $data['airlines'] = Airline::all();
        $this->layout->content = View::make('admin.airlines.index',$data);
    }
    
    /**
     * Return form to create new airlines
     */
    public function getNewForm()
    {
        $this->layout->content = View::make('admin.airlines.newAirlines');
    }
    
    /**
     * Save the contents of airlines
     */
    public function saveRecord()
    {
        $id = Input::get('id');
        $validator = Validator::make(Input::all(),
                                    array(
                                        'airline' => 'required|max:100 ',
                                        'address' => 'required|max:100',
                                        'email' => 'email'   
                                    )
                        );
        if($validator->fails()){
            if($id == ""){
                return Redirect::route('new-airline')->withErrors($validator->messages())->withInput();
            }else{
                return Redirect::route('edit-airline',$id)->withErrors($validator->messages())->withInput();
            }
        }
        else{
            $airline = new Airline();
            if($id == ""){
                $msg = 'New airline company inserted successfully !';
                
            }else{
                $airline = Airline::find($id);
                $msg = 'Airline details updated successfully !';
            }
            
            $airline->airlines = Input::get('airline');
            $airline->address = Input::get('address');
            $airline->phone = Input::get('phone');
            $airline->email_id = Input::get('email');
            $airline->save();
            
            Session::flash('success-message', $msg);
            return Redirect::route('list-airlines');
        }
       
    }
    
     /**
     * Load the view for specific airlines for edit purpose
     */
    public function getRecord($id){
       return $view= View::make('admin.airlines.newAirlines')->with('airline', Airline::find($id));
    }
    
}

/*END OF AIRLINE CONTROLLER*/
