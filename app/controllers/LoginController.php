<?php
class LoginController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Login 
     */
    public function login()
    {
        $validator = Validator::make(Input::all(),
                                    array(
                                        'username' => 'required',
                                        'password' => 'required'
                                    )
                        );
        if($validator->fails()){
                return Redirect::route('admin-login')->withErrors($validator->messages())->withInput();
        }
        else{
            $username = Input::get('username');
            $password = Input::get('password');
            
            $auth = Auth::attempt(array('username' => $username , 'password' => $password));
            if($auth){
                 return Redirect::intended('admin_access');
            }else{
                $error_msg = "Wrong combination of username and password.";
                Session::flash('error-message', $error_msg);
                return Redirect::route('admin-login')->withInput(); 
            }  
        }
    }
    
    /**
     * Logout 
     */
    public function logout(){
        Auth::logout();
        $msg = 'You have been successfully logged out';
        Session::flash('logout-message', $msg);
        return Redirect::to('_cpanel'); 
        
    }
    
}

/*END OF LOGIn CONTROLLER*/
