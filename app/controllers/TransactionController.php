<?php
class TransactionController extends BaseController {

    protected $layout = 'layouts.master';

    /**
     * Show list of clients
     */
    public function index()
    {
        $data['transactions'] = Transaction::with('client','category')->orderBy('transaction_date', 'DESC')->get();
        $this->layout->content = View::make('admin.transaction.index',$data);
    }
    
    /**
     * Return form to create new transaction
     */
    public function getNewForm()
    {
        $this->layout->content = View::make('admin.transaction.newtransaction');
    }
    
    /**
     * Return form to create new clients
     */
    public function saveTransaction()
    {

        $validator = Validator::make(Input::all(),
                                    array(
                                        'transaction_date' => 'required',
                                        'amount' => 'numeric|required' 
                                    )
                        );
        if($validator->fails()){
                return Redirect::route('new-transaction')->withErrors($validator->messages())->withInput();
        }
        else{
            $transaction = new Transaction();
            
            $transaction->transaction_date = date('Y-m-d',strtotime(Input::get('transaction_date')));
            $transaction->amount = Input::get('amount');
            $transaction->payment_type = Input::get('payment_type');
            
            $transaction_type = Input::get('transaction_type');
            
            if($transaction_type == "income"){
                $transaction_no = "inc_".str_random(20);
                $category_id = Input::get('income_from');
            }else{
                $transaction_no = "exp_".str_random(20);
                //$category_id = Input::get('expense_for');
                $category_id = "N/A";
            }
            
            $payment_type = Input::get('payment_type');
            if($payment_type == "Cheque"){
                $cheque_no = Input::get('voucher_no');
                $voucher_no = "N/A";
            }
            elseif($payment_type == "Deposit"){
                $voucher_no = Input::get('voucher_no');
                $cheque_no = "N/A";
            }else{
                $voucher_no = "N/A";
                $cheque_no = "N/A";
            }
            
            if(Input::get('client') == ""){
                $client_id = null;
            }else{
                $client_id = Input::get('client');
            }
            
            if(Input::get('ticket') == ""){
                $ticket_id = null;
            }else{
                $ticket_id = Input::get('client');
            }
            $transaction->type = $transaction_type;
            $transaction->transaction_no = $transaction_no;
            $transaction->category_id = $category_id;
            
            $transaction->ticket_id = $ticket_id;
            $transaction->client_id = $client_id; 
            
            $transaction->voucher_no = $voucher_no;
            $transaction->cheque_no = $cheque_no;
            
            $transaction->remarks = Input::get('remarks');
            
            $transaction->save();
            
            $msg = 'New transaction record created successfully !';
            Session::flash('success-message', $msg);
            return Redirect::route('list-transaction');
        }
    }
    
    /**
     * Return client list for selection through ajax
     */
    public function getClient(){
        $data['clients'] = Client::all();
        return View::make('admin.ajax.clientlist',$data);
    }

    
    /**
     * Return Category 
     */
    public function getSource(){
        $type = Input::get('type');//client or other
        $fetchData = Input::get('fetchData');//income or expense
        
        $data['categories'] = Category::where('type',$fetchData)->where('subtype',$type)->get();
        
        return View::make('admin.ajax.sourcelist',$data);
    }
    
    /**
     * Return Category 
     */
    public function getTicket(){
        $client_id = Input::get('client_id');
        
        $data['tickets'] = Ticket::where('client_id',$client_id)->get();
        
        return View::make('admin.ajax.ticketlist',$data);
    }
    
    
}

/*END OF client CONTROLLER*/
