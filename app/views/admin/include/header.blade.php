<head>
    <meta charset="UTF-8">
    <title>Annapurna Travel Software</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{ URL:: asset('admin_files/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{ URL:: asset('admin_files/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ URL:: asset('admin_files/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="{{ URL:: asset('admin_files/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL:: asset('admin_files/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="{{ URL:: asset('admin_files/css/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href=" {{ URL:: asset('admin_files/css/jQueryUI/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('admin_files/js/bootstrap-timepicker-gh-pages/css/bootstrap-timepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('admin_files/css/jquery.chosen.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('admin_files/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('plugin/nailthumb/jquery.nailthumb.1.1.css') }}">

    <script src="{{ URL:: asset('admin_files/js/jquery.js') }}" type="text/javascript"></script>
    <!-- jQuery UI 1.10.3 -->
</head>

<body class="skin-blue">
    <!-- header logo: style can be found in header.less -->
    <header class="header">
        <a href="#" class="logo">
            	<img src="{{ URL:: asset('admin_files/img/logo_sunbi.png') }}" style="padding-bottom:5px;" /> 
            </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                            <span>Admin<i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">Log out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
