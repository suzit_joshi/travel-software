<html>
@include('admin/include/header')
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!---left_panel-->
    @include('admin/include/navigation')
    <!--END LEFT PANEL-->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Customers
            <small>Manage your customers and transaction</small>
        </h1>
            <ol class="breadcrumb">
                <li><a href=><i class="fa fa-home"></i> Home</a>
                </li>
                <li class="active">Messages</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class='row'>
                <div class='col-md-12'>
                    <div class='box'>
                        <div class="box-header">
                            <h3 class="box-title">New Message</h3>
                        </div>
                        <div class='box-body pad'>

                            <div class="form-group">
                                <label for="msg_from">Message From</label>
                                <input type="text" class="form-control" name="msg_from" required="required" placeholder="Enter Message From">
                            </div>

                            <div class="form-group">
                                <label for="designation">Designation</label>
                                <input type="text" class="form-control" name="designation" required="required" placeholder="Enter designation">
                            </div>

                            <div class="form-group">
                                <label for="image_upload">Upload Image</label>
                                <input type="file" id="image_upload" name="userfile">
                                <p class="help-block">Please upload image</p>
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="textarea" name="description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </aside>

</div>
<div class="clear"></div>

<!--footer-->
@include('admin/include/footer')
<!--footer-->

</body>

</html>