@extends('layouts.master') 
@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Clients
            <small>Manage your clients and transaction</small>
        </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::to('admin')}}"><i class="fa fa-home"></i> Home</a>
                </li>
                <li class="active">Customers</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class='row'>
                <div class='col-md-12'>
                    <div class='box'>
                        <div class="box-header">
                            <h3 class="box-title">Register Client</h3>
                        </div>
                        <div class='box-body pad'>
                            {{ Form::open(array('url' => 'register')) }}
                            
                                <div class="form-group">
                                    {{ Form::label('name','Full Name')}}
                                    {{ Form::text( 'name', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Full Name'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('address','Address')}}
                                    {{ Form::text( 'address', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Address'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('passport_no','Passport No')}}
                                    {{ Form::text( 'passport_no', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Passport No'
                                                                      ) 
                                                 )
                                    }}
                                </div>
            
                                <div class="form-group">
                                    {{ Form::label('email','Email Address')}}
                                    {{ Form::text( 'email', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Email Id'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('phone','Phone Number')}}
                                    {{ Form::text( 'phone', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Phone No'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('username','Username')}}
                                    {{ Form::text( 'username', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Username'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('password','Default password')}}
                                    {{ Form::password('password', array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Set default password'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('re_password','Re-Enter Default password')}}
                                    {{ Form::password('re_password', array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Set re-enter default password'
                                                                      ) 
                                                 )
                                    }}
                                </div>


                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </aside>

@stop