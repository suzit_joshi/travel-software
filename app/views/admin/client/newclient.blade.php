@extends('layouts.master') 
@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Client
            <small>Manage your clients</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::route('admin')}}"><i class="fa fa-home"></i> Home</a>
                </li>
                <li class="active">Client</li>
            </ol>
        </section>
        @include('layouts/notification')

        <!-- Main content -->
        <section class="content">
            <div class='row'>
                <div class='col-md-12'>
                    @if(isset($airline))
                        <div class='box'>
                        <div class="box-header">
                            <h3 class="box-title">Edit Airlines Company</h3>
                        </div>
                        <div class='box-body pad'>
                            {{ Form::open(array('route' => 'save-airline')) }}
                            {{ Form::hidden('id',"$airline->id" )}}
                            {{ Form::token() }}
                                <div class="form-group">
                                    {{ Form::label('airline','Airlines Company')}}
                                    {{ Form::text( 'airline', "$airline->airlines" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Name of Airlines Company',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('address','Address')}}
                                    {{ Form::text( 'address', "$airline->address" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Company Address',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('phone','Phone No')}}
                                    {{ Form::text( 'phone', "$airline->phone" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Phone No'
                                                                      ) 
                                                 )
                                    }}
                                </div>
            
                                <div class="form-group">
                                    {{ Form::label('email','Email Address')}}
                                    {{ Form::text( 'email', "$airline->email_id" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Email Id'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                    
                            </div>


                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" value="Save airline">Save Changes</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    @else
                        <div class='box'>
                        <div class="box-header">
                            <h3 class="box-title">Register Clients</h3>
                        </div>
                        <div class='box-body pad'>
                            {{ Form::open(array('route' => 'save-client')) }}
                            
                            {{ Form::token() }}
                                <div class="form-group">
                                    {{ Form::label('client','Client Name')}}
                                    {{ Form::text( 'client', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Full Name of Client',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('address','Address')}}
                                    {{ Form::text( 'address', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Company Address',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('phone','Phone No')}}
                                    {{ Form::text( 'phone', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Phone No'
                                                                      ) 
                                                 )
                                    }}
                                </div>
            
                                <div class="form-group">
                                    {{ Form::label('email','Email Address')}}
                                    {{ Form::text( 'email', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Email Id'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('passport','Passport No')}}
                                    {{ Form::text( 'passport', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Passport No',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('username','Username')}}
                                    {{ Form::text( 'username', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Username',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('password','Default Password')}}
                                    {{ Form::password( 'password', array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Default Password',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('password_again','Default Password')}}
                                    {{ Form::password('password_again', array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Default Password Again',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                    
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" value="Save airline">Save Changes</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    @endif
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </aside>

@stop