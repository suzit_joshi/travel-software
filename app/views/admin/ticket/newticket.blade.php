@extends('layouts.master') 
@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Tickets
            <small>Manage tickets for clients</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::route('admin')}}"><i class="fa fa-home"></i> Home</a>
                </li>
                <li class="active">Tickets</li>
            </ol>
        </section>
        @include('layouts/notification')

        <!-- Main content -->
        <section class="content">
            <div class='row'>
                <div class='col-md-12'>
                    @if(isset($airline))
                        <div class='box'>
                        <div class="box-header">
                            <h3 class="box-title">Edit Airlines Company</h3>
                        </div>
                        <div class='box-body pad'>
                            {{ Form::open(array('route' => 'save-airline')) }}
                            {{ Form::hidden('id',"$airline->id" )}}
                            {{ Form::token() }}
                                <div class="form-group">
                                    {{ Form::label('airline','Airlines Company')}}
                                    {{ Form::text( 'airline', "$airline->airlines" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Name of Airlines Company',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('address','Address')}}
                                    {{ Form::text( 'address', "$airline->address" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Company Address',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('phone','Phone No')}}
                                    {{ Form::text( 'phone', "$airline->phone" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Phone No'
                                                                      ) 
                                                 )
                                    }}
                                </div>
            
                                <div class="form-group">
                                    {{ Form::label('email','Email Address')}}
                                    {{ Form::text( 'email', "$airline->email_id" , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Email Id'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                    
                            </div>


                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" value="Save airline">Save Changes</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    @else
                        <div class='box'>
                        <div class="box-header">
                            <h3 class="box-title">Ticketing for Clients</h3>
                        </div>
                        <div class='box-body pad'>
                            {{ Form::open(array('route' => 'save-ticket')) }}
                            
                            {{ Form::token() }}
                                <div class="form-group">
                                    {{ Form::label('client','Client Name')}}
                                                                                  
                                    @if(isset($clients))
                                        {{  Form::select('client',$clients , null , array('class' => 'chzn-select form-control',
                                                                                          'required' => 'required'
                                                                                    ) 
                                                        ) 
                                        }}  
                                    @else
                                        {{  Form::select('client', array('' => 'No Clients till now' ), null ,
                                                                  array('class' => 'form-control','required' => 'required')
                                                        ) 
                                        }}                                     
                                    @endif
                                     
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('airlines','Airline Company')}}
                                                                                  
                                    @if(isset($airlines))
                                        {{  Form::select('airlines',$airlines , null , array('class' => 'chzn-select form-control',
                                                                                          'required' => 'required'
                                                                                    ) 
                                                        ) 
                                        }}  
                                    @else
                                        {{  Form::select('client', array('' => 'No airlines till now' ), null ,
                                                                  array('class' => 'form-control','required' => 'required')
                                                        ) 
                                        }}                                     
                                    @endif
                                    
                                </div>
                                
                            
                                <div class="form-group">
                                    {{ Form::label('ticket_no','Ticket No')}}
                                    {{ Form::text( 'ticket_no', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Ticket No'
                                                                      ) 
                                                 )
                                    }}
                                </div>
            
                                <div class="form-group">
                                    {{ Form::label('departure','Departing From')}}
                                    {{ Form::text( 'departure', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter departure from'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('destination','Destination')}}
                                    {{ Form::text( 'destination', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Destination',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('charge','Total Fare')}}
                                    {{ Form::text( 'charge', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Total Charge for Ticketing',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('advance','Advance Paid')}}
                                    {{ Form::text( 'advance', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Advance paid',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            
                                <div class="form-group">
                                    {{ Form::label('due','Due Amount')}}
                                    {{ Form::text( 'due', '' , array(
                                                                        'class'=>'form-control',
                                                                        'readonly' => 'readonly'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                                
            
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" value="Save airline">Save Changes</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    @endif
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </aside>

@stop