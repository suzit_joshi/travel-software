@extends('layouts.master') 
@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Income/Expense Headings
                <small>Manage your categories for income and expenses</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::route('admin')}}"><i class="fa fa-home"></i> Home</a>
                </li>
                <li class="active"> Income/Expense Headings</li>
            </ol>
        </section>

        @include('layouts/notification')
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="box-title">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#new-category">
                                    Add New</button>
                            </div>
                        </div>
                        

                        <div class="box-body table-responsive">
                            <div class="row">
                                <div class="col-md-12 category-tab">
                                    <!-- Custom Tabs -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">Income</a>
                                            </li>
                                            <li><a href="#tab_2" data-toggle="tab">Expense</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <table id="table1" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:2% !important;">S.No</th>
                                                            <th>Income Heading</th>
                                                            <th>Type</th>
                                                            <th>Option</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="bank-record">
                                                        <?php 
                                                           $counter=1 ; 
                                                        ?>
                                                        @foreach($income_category as $row)
                                                        
                                                        <tr>
                                                            <td>
                                                                {{ $counter }}
                                                            </td>
                                                            <td class="category-name" data-id="<?/*$id*/?>" 
                                                                category-heading="<?/*$record->subtype*/?>" type="income">
                                                                <span class="name">{{ $row->category_title }}</span>
                                                            </td>
                                                            <td>{{ ucfirst($row->subtype) }}</td>
                                                            <td> 

                                                                    <div class="btn-group">
                                                                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                          <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                      </button>
                                                                      <ul class="dropdown-menu pull-right" role="menu">
                                                                            <li> <a href="#" class="edit-category">Edit</a></li>
                                                                            <li class="divider"></li>
                                                                            <li><a href="#" 
                                                                            onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                        </ul>
                                                                    </div>
                                                            </td>
                                                        </tr>
                                                        <?php $counter+=1; ?>
                                                       @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <table id="table2" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:2% !important;">S.No</th>
                                                            <th>Expense Category</th>
                                                            <th>Type</th>
                                                            <th>Option</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php   $counter=1 ; ?>
                                                        @foreach($expense_category as $row)
                                                        <tr>
                                                            <td>
                                                                {{ $counter }}
                                                            </td>
                                                            <td class="category-name" data-id="<?/*$id*/?>" 
                                                                category-heading="<?/*$record->subtype*/?>" type="expense">
                                                                <span class="name">{{ $row->category_title }}</span>
                                                            </td>
                                                            <td>{{ $row->subtype }} </td>
                                                            <td> 
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li> <a href="#" class="edit-category">Edit</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#" 
                                                                        onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $counter+=1; ?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.tab-pane -->

                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- nav-tabs-custom -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>

                </div>
            </div>
            </div>
            <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </aside>

    <!-- Modal -->
    <div class="modal fade" id="new-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">New Income/Expense Category</h4>
                </div>
                {{ Form::open(array('route' => 'save-category','class' => 'form-horizontal')) }}
                            
                {{ Form::token() }}
                    <div class="modal-body">
                        <div class="form-group">
                            {{ Form::label('type','Select type', array('class' => 'col-sm-4'))}}
                            <div class="col-sm-8">
                                {{  Form::select('type',array('income'=>'Income', 'expense' => 'Expense'), 
                                                        null , array('class' => 'form-control', 'required' => 'required') 
                                                        ) 
                                }}
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            {{ Form::label('sub_type','Select label', array('class' => 'col-sm-4'))}}
                            <div class="col-sm-8">
                                {{  Form::select('sub_type',array('client'=>'Client', 'other' => 'Other'), 
                                                        null , array('class'=> 'form-control','required' => 'required') 
                                                        ) 
                                }}
                            </div>
                        </div>
                                
                        <div class="form-group">
                            {{ Form::label('title','Category Title', array('class' => 'col-sm-4'))}}
                            <div class="col-sm-8">
                                {{ Form::text( 'title', '' , array(
                                                                    'class'=>'form-control',
                                                                    'placeholder' => 'Enter Category Title'
                                                                  ) 
                                                 )
                                }}
                            </div>
                        </div>
                        
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                {{ Form::close() }}
                <!--END FORM-->

            </div>
        </div>
    </div>

    <!--MODAL TO EDIT-->
    <div class="modal fade" id="edit-category-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Edit Income/Expense Category</h4>
                </div>
                <form class="form-horizontal" role="form" id="edit-submit-category">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-4">Select Type</label>
                            <input type="hidden" id="category_id">
                            <div class="col-sm-8">
                                <p class="form-control-static" id="category-type"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4">Select label</label>
                            <div class="col-sm-8">
                                <select id="edit-category-heading" class="form-control">
                                    <option value="Candidate">Ticket</option>
                                    <option value="Other">Others</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4">Category Title</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="edit-category">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Save changes</button>
                    </div>
                </form>
                <!--END FORM-->

            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on('submit','#submit-category', function(e){
            e.preventDefault();
            var category_type = $('.type option:selected').val();
            var category_title = $('#category').val();
            var categoryHeading = $('#category-heading option:selected').val(); 
            $.ajax({
                type: "POST",
                url: "{{ URL::route('ajax-save-category')}}",
                data: {
                    category_type: category_type,
                    category_title: category_title,
                    category_heading: categoryHeading
                },
                success: function (data) {
                    $('.category-tab').html(data);
                    $('#new-category').modal('hide');
                    $('#success-alert').slideDown();
                }
            });
        });

    </script>
@stop