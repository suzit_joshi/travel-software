@extends('layouts.master') 
@section('content')
<style>
    .label-block{
        display:block;
    }

    .voucher-no,
    #expense-form
    {
        display:none;
    }
    

</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transactions
            <small>Manage your transactions</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('admin') }}"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active"> Income/Expense Headings</li>
        </ol>
    </section>

    @include('layouts/notification')

    <!-- Main content -->
    <section class="content" style="height:auto !important;">
        <div class='row'>
            <div class='col-md-12'>

                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">New Transaction</h3>
                    </div>
                    <div class='box-body pad'>
                        {{ Form::open(array('route' => 'save-transaction')) }}
                             
                        {{ Form::token() }}

                            <div class="row">
                                <div class="form-group col-md-6">
                                    {{ Form::label('transaction_type','Select Transaction Type', array('class' => 'label-block'))}}
                                    <label class="radio-inline">
                                        {{ Form::radio('transaction_type','income',true, array('class' => 'transaction-type'))}} Income
                                    </label>
                                    <label class="radio-inline">
                                        {{ Form::radio('transaction_type','expense',false, array('class' => 'transaction-type'))}} Expense
                                    </label>
                                </div>

                                <div class="form-group col-md-6">
                                    {{ Form::label('transaction_date','Transaction Date')}}
                                    {{ Form::text( 'transaction_date', date('d-M-Y') , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Transaction Date',
                                                                        'id' => 'datepicker',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('amount','Amount')}}
                                {{ Form::text( 'amount', '' , array(
                                                                        'class'=>'form-control',
                                                                        'placeholder' => 'Enter Transaction Amount',
                                                                        'id' => 'amount',
                                                                        'required' => 'required'
                                                                      ) 
                                                 )
                                    }}
                            </div>

                            <div class="form-group">
                                   {{ Form::label('payment_type','Payment Type', array('class' => 'label-block'))}}
                                    <label class="radio-inline">
                                       {{ Form::radio('payment_type','Cash',true, array('class' => 'payment-type'))}} Cash
                                    </label>

                                    <label class="radio-inline">
                                       {{ Form::radio('payment_type','Cheque',false, array('class' => 'payment-type'))}} Cheque
                                    </label>
                                    <label class="radio-inline">
                                       {{ Form::radio('payment_type','Deposit',false, array('class' => 'payment-type'))}} Deposit
                                    </label>
                            </div>
                            
                            <div class="form-group voucher-no">
                                    <div class="row">
                                        {{ Form::label('voucher_no','Cheque/Voucher No', array('class' => 'col-md-2'))}}
                                        <div class="col-md-4">
                                            {{ Form::text('voucher_no','',array(
                                                                               'class'=>'form-control',
                                                                               'placeholder' => 'Enter Voucher No'
                                                                            )
                                                         )
                                            }}
                                        </div>
                                    </div>
                            </div>
                            
                            <!--Income Form-->
                            <div id="income-form">    

                                <div class="form-group">
                                     {{ Form::label('income_type','Income Category')}}
                                        {{  Form::select('income_type',array(
                                                                           '' => 'Select One',
                                                                           'client'=>'Client', 
                                                                           'other' => 'Other'),
                                                                     null , 
                                                                     array('class'=> 'form-control',
                                                                            'id' => 'income-category'
                                                                            ) 
                                                        ) 
                                        }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('client','Income Source - Clients')}}
                                    {{  Form::select('client',array('' => 'Select One'),
                                                                     null , 
                                                                     array('class'=> 'form-control',
                                                                            'id' => 'income-client'
                                                                            ) 
                                                        ) 
                                        }}
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('ticket','Client Ticket')}}
                                    {{  Form::select('ticket',array('' => 'Select One'),
                                                                     null , 
                                                                     array('class'=> 'form-control',
                                                                            'id' => 'income-ticket'
                                                                            ) 
                                                        ) 
                                        }}
                                </div>

                                <div class="form-group">
                                   {{ Form::label('income_from','Income From')}}
                                    {{  Form::select('income_from',array('' => 'Select One'),
                                                                     null , 
                                                                     array('class'=> 'form-control',
                                                                            'id' => 'income-from'
                                                                            ) 
                                                        ) 
                                        }}
                                </div>

                            </div>
                            <!--END Income Form-->
                            
                            <!--Expense Form-->
                            <div id="expense-form">
                                <div class="form-group">
                                     {{ Form::label('expense_type','Expense Category')}}
                                        {{  Form::select('expense_type',array(
                                                                           '' => 'Select One',
                                                                           'ticket'=>'Ticket', 
                                                                           'other' => 'Other'),
                                                                     null , 
                                                                     array('class'=> 'form-control',
                                                                            'id' => 'expense-category'
                                                                            ) 
                                                        ) 
                                        }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('expense_client','Expense For - Clients')}}
                                    {{  Form::select('expense_client',array('' => 'Select One'),
                                                                     null , 
                                                                     array('class'=> 'form-control',
                                                                            'id' => 'expense-client'
                                                                            ) 
                                                        ) 
                                        }}
                                </div>

                                <div class="form-group">
                                   {{ Form::label('expense_from','Expense From')}}
                                    {{  Form::select('expense_from',array('' => 'Select One'),
                                                                     null , 
                                                                     array('class'=> 'form-control',
                                                                            'id' => 'expense-from'
                                                                            ) 
                                                        ) 
                                        }}
                                </div>

                            </div>
                            <!--END Income Form-->




                            <div class="form-group">
                                {{ Form::label('remarks','General Remarks (if any)')}}
                                {{ Form::textarea('remarks','',array('class' => 'textarea text-area-style'))}}
                            </div>

                            <div class="box-footer">
                                {{ Form::button('Save Changes',array('class' => 'btn btn-primary','type' => 'submit'))}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
<script type="text/javascript">
$(function () {
    $("#datepicker").datepicker({
        dateFormat: "dd-M-yy",
        showAnim: "blind"
    });
    });
    
    /*Adding .00 to amount*/
    $('#amount').on('blur', function (e) {
        amount = $(this).val();
        var a = ".";
        if(amount.indexOf(a) == -1){
            newAmt = amount + ".00";
            $('#amount').val(newAmt);
        }
    });
    
    /*Displaying form according to transaction type*/
    $('.transaction-type').on('ifChecked', function (event) {
        formType = $(this).val();
        formDisplay(formType);
    });

    function formDisplay(type) {
        if (type == 'income') {
            $('#expense-form').slideUp(function () {
                $('#income-form').slideDown();
            });
            $('#form-submit').removeClass('expense');
        } else{
            $('#income-form').slideUp(function () {
                $('#expense-form').slideDown();
            });
            $('#form-submit').addClass('expense');
        } 
    }
    /*END Displaying form according to transaction type*/
    
    /*Displaying Voucher No form element according to condition*/
    $('.payment-type').on('ifChecked', function (event) {
        paymentType = $(this).val();
        if (paymentType == 'Cheque' || paymentType == 'Deposit') {
            $('.voucher-no').slideDown();
        } else {
            $('.voucher-no').slideUp();
            $('.bank-list').hide();
        }
    });
    /*END Displaying Voucher No form element according to condition*/
    
    
    /*TO GET LIST OF CANDIDATES AND INCOME CATEGORY */
    $(document).on('change', '#income-category', function () {
        var categoryVal = $('#income-category option:selected').html().toLowerCase();
        if(categoryVal == "client"){
            $.ajax({
                type: "POST",
                url: "{{ URL::route('get-client') }}",
                data: {
                    type: categoryVal,
                    fetchData : 'client'
                },
                success: function(data){

                    var abce = data.trim();
                    if (abce == "a") {
                        $('#income-client').html("<option value='n/a'>Not Applicable</option>");
                    } else {
                        $('#income-client').html(data);
                    }
                }
            });
        }
        else{
            $('#income-client').html("<option value='n/a'>Not Applicable</option>");
        }

       $.ajax({
            type: "POST",
            url: "{{ URL::route('get-source') }}",
            data: {
                type: categoryVal,
                fetchData : 'income'
            },
            success: function(adata){
                adata.trim();
                if (adata == "a") {
                    $('#income-from').html("<option value='n/a'>Not Applicable</option>");
                } else {
                    $('#income-from').html(adata);
                }
            }
        });
    });
    /*END LIST OF CANDIDATES AND INCOME CATEOGRY*/
    
    /*AJAX REQUEST TO GET LIST OF CLIENT'S RESPECTIVE TICKETS*/
    $(document).on('change','#income-client',function(){
        var clientId = $('#income-client option:selected').val();
        if(clientId == "n/a"){
           $('#income-ticket').html("<option value='n/a'>Not Applicable</option>"); 
        }
        else{
            $.ajax({
                type: "POST",
                url: "{{ URL::route('get-ticket') }}",
                data: {
                    client_id: clientId
                },
                success: function(adata){ 
                    adata.trim();
                    if (adata == "a") {
                        $('#income-ticket').html("<option value='n/a'>Not Applicable</option>");
                    } else {
                        $('#income-ticket').html(adata);
                    }
                }
            });
        }
    });
    /*END AJAX REQUEST TO GET LIST OF CLIENT'S RESPECTIVE TICKETS*/
    
</script>
@stop