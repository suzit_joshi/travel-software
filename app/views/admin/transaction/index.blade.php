@extends('layouts.master') 
@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transactions
            <small>Manage your transactions</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('admin')}}"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active"> Income/Expense Headings</li>
        </ol>
    </section>

    @include('layouts/notification')
    <!-- Main content -->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <div class='box'>
                    <div class="box-header">
                        <div class="box-title">
                            <button class="btn btn-primary" 
                                onClick="javascript:location.replace('{{ URL::route('new-transaction')}}')">
                            Add New</button>
                        </div>
                    </div>
                    <div class="box-body table-responsive">

                        <div class="row">
                            <div class="col-md-12">
                                <!-- Custom Tabs -->
                                <table id="table1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="3%">S.No</th>
                                            <th>Date</th>
                                            <th>Transaction title</th>
                                            <th>Type</th>
                                            <th>Payment Type</th>
                                            <th>Amount</th>
                                            <th>Client</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($transactions))
                                            <?php $i=1;?>
                                            @foreach($transactions as $row)
                                                
                                                <tr>
                                                    <td><?php echo $i++; ?></td>
                                                    <td>{{ date('d-M-Y',strtotime($row->transaction_date)) }}</td>
                                                    <td>{{ $row->category->category_title }}</td>
                                                    <td>{{ ucfirst($row->type) }}</td>
                                                    <td> {{ $row->payment_type }}</td>
                                                    <td> {{ "Rs. " .number_format($row->amount,2) }}</td>
                                                    <td> {{ $row->client->client_name }}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu pull-right" role="menu">
                                                                <li><a href="{{ URL::route('edit-airline',$row->id) }}">Edit</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
@stop