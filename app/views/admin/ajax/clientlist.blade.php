@if($clients)
   <option value="n/a">Select specific clients</option>
    @foreach($clients as $row)
    <option value="{{ $row->id }}">{{ $row->client_name }}</option>
    @endforeach
@else
    <option value="n/a">No Client Record</option>
@endif