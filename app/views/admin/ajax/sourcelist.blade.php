@if($categories)
    @foreach($categories as $row)
    <option value="{{ $row->id }}">{{ $row->category_title }}</option>
    @endforeach
@else
    <option value="">No Client Record</option>
@endif