@if($tickets)
    @foreach($tickets as $row)
    <option value="{{ $row->id }}">{{ $row->ticket_no }} {{ $row->departure." to ". $row->destination }}</option>
    @endforeach
@else
    <option value="n/a">No Ticket Record</option>
@endif