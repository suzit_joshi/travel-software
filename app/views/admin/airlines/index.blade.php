@extends('layouts.master') 
@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Airlines
            <small>Manage your airlines</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('admin')}}"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Airlines</li>
        </ol>
    </section>

    @include('layouts/notification')
    <!-- Main content -->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <div class='box'>
                    <div class="box-header">
                        <div class="box-title">
                            <button class="btn btn-primary" 
                                onClick="javascript:location.replace('{{ URL::route('new-airline')}}')">
                            Add New</button>
                        </div>
                    </div>
                    <div class="box-body table-responsive">

                        <div class="row">
                            <div class="col-md-12">
                                <!-- Custom Tabs -->
                                <table id="table1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="3%">S.No</th>
                                            <th>Airlines</th>
                                            <th>Contact no</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($airlines))
                                            <?php $i=1;?>
                                            @foreach($airlines as $row)
                                                
                                                <tr>
                                                    <td><?php echo $i++; ?></td>
                                                    <td>{{ $row->airlines}}</td>
                                                    <td>{{$row->phone}}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu pull-right" role="menu">
                                                                <li><a href="{{ URL::route('edit-airline',$row->id) }}">Edit</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
@stop