<html>

<head>
    <title>Cpanel Login - Annapurna Travels</title>
    <!-- bootstrap 3.0.2 -->
    <link href="{{ URL::asset('assets/admin_files/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{ URL::asset('assets/admin_files/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
</head>
<style>
    .login {
        position: absolute;
        height: 389px;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 25px;
        box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.2);
    }
</style>

<body>
    <div class="col-md-4 col-md-offset-4 login">
        <img src="{{ URL::asset('assets/admin_files/img/sunbi_logo.png') }}" 
             style="margin:0 auto;display:block" class="img-responsive" />

            @if ( $errors->count() > 0 )
                <div class="alert alert-info alert-dismissable">
                    <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>NOTICE!!</b>     
                    <ul>
                        @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                            @endforeach
                    </ul>
                </div>
            @endif
            
            @if(Session::has('error-message'))
                    <p style="color:red;">
                        <b>Error!!</b>  {{ Session::get('error-message') }}
                    </p>
            @endif
            
            @if(Session::has('logout-message'))
                    <p style="color:green;">
                        <b>Success!!</b>  {{ Session::get('logout-message') }}
                    </p>
            @endif

        
        {{ Form::open(array('route' => $type)) }}
        {{ Form::token() }}
        
        <div class="form-group">
            {{ Form::label('username','USERNAME')}}
            {{ Form::text( 'username', '' , array(
                                                'class'=>'form-control',
                                                'placeholder' => 'Username',
                                                'required' => 'required'
                                                ) 
                            ) }}
        </div>
        
        <div class="form-group">
            {{ Form::label('password','PASSWORD')}}
            {{ Form::password('password', array(
                                                'class'=>'form-control',
                                                'placeholder' => '**********',
                                                'required' => 'required'
                                                ) 
                            ) }}
        </div>
       
        <button type="submit" class="btn btn-primary">Login</button>
        {{ Form::close() }}
    </div>
</body>

</html>