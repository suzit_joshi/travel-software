<head>
    <meta charset="UTF-8">
    <title>Annapurna Travel Software</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{ URL:: asset('assets/admin_files/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{ URL:: asset('assets/admin_files/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ URL:: asset('assets/admin_files/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="{{ URL:: asset('assets/admin_files/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL:: asset('assets/admin_files/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="{{ URL:: asset('assets/admin_files/css/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href=" {{ URL:: asset('assets/admin_files/css/jQueryUI/jquery-ui.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('assets/admin_files/js/bootstrap-timepicker-gh-pages/css/bootstrap-timepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('assets/admin_files/css/jquery.chosen.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('assets/admin_files/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL:: asset('assets/plugin/nailthumb/jquery.nailthumb.1.1.css') }}">


    <script src="{{ URL:: asset('assets/admin_files/js/jquery.js') }}" type="text/javascript"></script>
    <!-- jQuery UI 1.10.3 -->
</head>


