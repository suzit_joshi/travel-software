<aside class="left-side sidebar-offcanvas"> 
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar"> 
		<!-- sidebar menu: : style can be found in sidebar.less -->
		
		<ul class="sidebar-menu">
			<li> <a href="#">Navigation Pane </a> </li>
			
			<li><a href="{{ URL::route('admin') }}"><i class="fa fa-home"></i> Dashboard </a> </li>
            
            @if(Auth::check())
                @if(Auth::user()->client_id == 0)
            
                    <li><a href="{{ URL::route('list-clients') }}"><i class="fa fa-user"></i> Client </a> </li>

                    <li><a href="{{ URL::route('list-airlines') }}"><i class="fa  fa-plane"></i> Airlines </a> </li>

                    <li><a href="{{ URL::route('list-tickets') }}"><i class="fa  fa-ticket"></i> Ticketing </a> </li>
                    
                    <li><a href="{{ URL::route('category') }}"><i class="fa fa-folder"></i>Category</a></li>
            
                    <li><a href="{{ URL::route('list-transaction') }}"><i class="fa fa-dollar"></i>Transactions </a></li> </li>
                
                @else
                    <li><a href="#"><i class="fa fa-dollar"></i>Your Transactions </a></li> </li>
                    
                @endif
                    <li><a href="#"> <i class="fa  fa-lock"></i>Change Password</a></li>
            @endif
			
		</ul>
	</section>
	<!-- /.sidebar --> 
</aside>