<html>
    @include('layouts/header')
    <body class="skin-blue">
    <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="#" class="logo">
                    <img src="{{ URL:: asset('assets/admin_files/img/logo_sunbi.png') }}" style="padding-bottom:5px;" /> 
                </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <span>{{ Auth::user()->username }}<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="{{ URL::route('logout') }}" class="btn btn-default btn-flat">Log out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        
        <div class="wrapper row-offcanvas row-offcanvas-left"> 
            <!---left_panel-->
            @include('layouts/navigation')
            <!--END LEFT PANEL-->
            @yield('content')       
        </div>
            
     @include('layouts/footer')
</html>