<?php
class Client extends Eloquent {

    public function transactions()
    {
        return $this->hasMany('Transaction');
    }
    
    public function tickets()
    {
        return $this->hasMany('Ticket');
    }

}