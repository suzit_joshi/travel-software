<?php
class Airline extends Eloquent {

    public function tickets()
    {
        return $this->hasMany('Ticket');
    }

}