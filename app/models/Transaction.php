<?php
class Transaction extends Eloquent {

    public function Category()
    {
        return $this->belongsTo('Category');
    }
    
    public function Client()
    {
        return $this->belongsTo('Client');
    }

}