<?php
class Ticket extends Eloquent {

    public function client()
    {
        return $this->belongsTo('Client');
    }

    public function airline()
    {
        return $this->belongsTo('Airline');
    }
}