<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('login')->with('type','admin-login');
});

/*ROUTE FOR LOGIN*/
Route::get('/_cpanel', function()
{
	return View::make('login')->with('type','admin-login');
});


Route::group(array('before' => 'csrf'), function(){
    /*Login*/
    Route::post('/_cpanel',array(
                        'as'=>'admin-login',
                        'uses'=> 'LoginController@login')
               );
    
});


Route::get('admin_access', array(
                    'as' => 'admin',
                    'uses' => 'AdminController@index'
        ));

/*Client Section Routes*/

Route::get('admin/clients', array(
                    'as' => 'list-clients',
                    'uses' => 'ClientController@index' 
        ));


Route::get('admin/newclients', array(
                    'as' => 'new-client',
                    'uses' => 'ClientController@getNewForm' 
        ));

Route::group(array('before' => 'csrf'), function(){
    /*Save Clients*/
    Route::post('admin/add-edit-client',array(
                'as' => 'save-client',
                'uses' => 'ClientController@saveRecord',
                ));
});


/*END CLIENT ROUTES*/
/***************************************************************/


/*AIRLINES ROUTES*/

Route::get('admin/airlines', array(
                    'as' => 'list-airlines',
                    'uses' => 'AirlineController@index' 
        ));

Route::get('admin/newairlines', array(
                    'as' => 'new-airline',
                    'uses' => 'AirlineController@getNewForm' 
        ));

Route::get('admin/editairlines/{id}',array(
                                'as' => 'edit-airline',
                                'uses' => 'AirlineController@getRecord'
            ));

Route::group(array('before' => 'csrf'), function(){
    /*Save Airlines*/
    Route::post('admin/add-edit-airlines',array(
                'as' => 'save-airline',
                'uses' => 'AirlineController@saveRecord',
                ));
});

/*Route::get('admin/editairlines/{$id}',array(
                'as' => 'edit-airline',
                'uses' => 'AirlineController@getRecord',
            ));*/



/*END AIRLINES ROUTES*/

/***************************************************************/

/*TICKETING ROUTES*/

Route::get('admin/tickets', array(
                    'as' => 'list-tickets',
                    'uses' => 'TicketController@index' 
        ));

Route::get('admin/newticket', array(
                    'as' => 'new-ticket',
                    'uses' => 'TicketController@getNewForm' 
        ));

Route::group(array('before' => 'csrf'), function(){
    /*Save Airlines*/
    Route::post('admin/new-ticket',array(
                'as' => 'save-ticket',
                'uses' => 'TicketController@saveRecord',
                ));
});


/*END TICKETING ROUTES*/

/***************************************************************/

/*TRANSACTION-CATEGORY ROUTES*/

Route::get('admin/transaction-categories', array(
                    'as' => 'category',
                    'uses' => 'CategoryController@index' 
        ));

Route::group(array('before' => 'csrf'), function(){
    /*Save Airlines*/
    Route::post('admin/add-edit-category',array(
                'as' => 'save-category',
                'uses' => 'CategoryController@saveCategory',
                ));
});



/*END TRANSACTION-CATEGORY ROUTES*/

/***************************************************************/

/*TRANSACTION ROUTES*/

Route::get('admin/transactions', array(
                    'as' => 'list-transaction',
                    'uses' => 'TransactionController@index' 
        ));

Route::get('admin/newtransaction', array(
                    'as' => 'new-transaction',
                    'uses' => 'TransactionController@getNewForm' 
        ));

Route::group(array('before' => 'csrf'), function(){
    Route::post('admin/save-transaction',array(
                        'as'=>'save-transaction',
                        'uses'=> 'TransactionController@saveTransaction')
               );
    
});

Route::post('admin/get-clients', array(
                    'as' => 'get-client',
                    'uses' => 'TransactionController@getClient' 
        ));

Route::post('admin/get-source', array(
                    'as' => 'get-source',
                    'uses' => 'TransactionController@getSource' 
        ));

Route::post('admin/get-ticket', array(
                    'as' => 'get-ticket',
                    'uses' => 'TransactionController@getTicket' 
        ));




/*END TRANSACTION ROUTES*/

/***************************************************************/
/*ROUTES TO HANDLE AJAX REQUESTS*/
Route::post('admin/new-category', array(
                    'as' => 'ajax-save-category',
                    'uses' => 'CategoryController@saveCategory' 
        ));


Route::get('logout',array(
                    'as'    => 'logout',
                    'uses'  => 'LoginController@logout'
        ));



