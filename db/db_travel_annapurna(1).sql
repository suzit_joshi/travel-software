-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 06, 2015 at 10:43 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_travel_annapurna`
--

-- --------------------------------------------------------

--
-- Table structure for table `airlines`
--

CREATE TABLE IF NOT EXISTS `airlines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `airlines` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `airlines_airlines_unique` (`airlines`),
  UNIQUE KEY `airlines_email_id_unique` (`email_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `airlines`
--

INSERT INTO `airlines` (`id`, `airlines`, `address`, `phone`, `email_id`, `created_at`, `updated_at`) VALUES
(1, 'Buddha Airlines Company', 'Shantinagar', '425789333', 'buddha@airlines.com', '0000-00-00 00:00:00', '2015-04-08 23:15:07'),
(2, 'Dummy Airline', 'dummy address', '56588888', 'dummy@gmail.com', '2015-04-08 01:47:47', '2015-04-08 23:15:40'),
(3, 'Royal Nepal Airline', 'NewRoad, Kathmandu', '54665487', 'royal.nepal@hotmail.com', '2015-04-08 03:04:45', '2015-04-08 03:04:45'),
(4, 'Yeti Airlines', 'Sinamangal', '42578965', 'yeti@gmail.com', '2015-04-08 04:03:27', '2015-04-08 04:03:27'),
(5, 'Shangrila Air', 'Lazimpat', '54654665', 'shangrila@nepal.com.np', '2015-04-08 04:18:51', '2015-04-08 04:18:51'),
(6, 'Thai Airlines', 'Thailand', '56889996999', 'thai@airways.com', '2015-04-08 23:09:27', '2015-04-08 23:09:27'),
(7, 'Shangri La Airlines', '19366 sw 132 ave', '7868992795', 'shangrila@hotmail.com', '2015-04-09 02:50:51', '2015-04-09 02:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_category_title_unique` (`category_title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `type`, `subtype`, `category_title`, `created_at`, `updated_at`) VALUES
(5, 'income', 'client', 'Advance Payment', '2015-05-05 23:13:12', '2015-05-05 23:13:12'),
(6, 'income', 'client', 'Deposit', '2015-05-05 23:13:24', '2015-05-05 23:13:24'),
(7, 'income', 'client', 'Full Payment', '2015-05-05 23:13:44', '2015-05-05 23:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passport` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `clients_email_id_unique` (`email_id`),
  UNIQUE KEY `clients_passport_unique` (`passport`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `address`, `phone`, `email_id`, `passport`, `created_at`, `updated_at`) VALUES
(1, 'Sujit Joshi', 'Dallue', '98456566656', 'its.suzit@gmail.com', '78879445', '2015-04-09 23:33:36', '2015-04-09 23:33:36'),
(2, 'Dileep Kumar Chaudhary', 'Kathmandu', '98656565656', 'mail@dileep.com.np', '8798798', '2015-04-12 23:31:43', '2015-04-12 23:31:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_04_03_085520_create_airlines', 1),
('2015_04_03_090059_create_session_table', 1),
('2015_04_03_090328_create_users', 2),
('2015_04_09_070916_create_clients_table', 3),
('2015_04_09_071655_create_clients', 4),
('2015_04_09_115056_create_users', 4),
('2015_04_13_043633_create_tickets_table', 5),
('2015_04_20_043401_create_transaction_category', 6),
('2015_04_20_074931_create_transaction_table', 7),
('2015_04_20_094047_create_transaction_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `departure` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clients_id` int(10) unsigned NOT NULL,
  `airlines_id` int(10) unsigned NOT NULL,
  `departure_time` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tickets_ticket_no_unique` (`ticket_no`),
  KEY `tickets_clients_id_foreign` (`clients_id`),
  KEY `tickets_airlines_id_foreign` (`airlines_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_date` datetime NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voucher_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cheque_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `clients_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `ticket_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `transactions_transaction_no_unique` (`transaction_no`),
  KEY `transactions_clients_id_foreign` (`clients_id`),
  KEY `transactions_category_id_foreign` (`category_id`),
  KEY `transactions_ticket_id_foreign` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `client_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$K5i2O61v09iVDbUPTrDwDO3ClaMnZHZDPiRW4ulbT4nyyYnSkiKTi', 0, 'yFYE2nYk2D1Mncax0aWi3TMW6g0kywucgihCWs0biIlm85EFH1JJDPGNSHDE', '0000-00-00 00:00:00', '2015-04-22 22:49:18'),
(2, 'suzit', '$2y$10$K5i2O61v09iVDbUPTrDwDO3ClaMnZHZDPiRW4ulbT4nyyYnSkiKTi', 1, 'hIgjT9TJkaHz4h0ywN3qT0Y5CK2Po3HNbIeRpEqHb22s9MLarkVer4LGDqfj', '2015-04-09 23:33:36', '2015-04-10 00:19:02'),
(3, 'd4dileep', '$2y$10$tZzkbtdHDgwM4M6T2pb5pOJrcSF3on6sncDmsRJQ3EAH50507xMru', 2, NULL, '2015-04-12 23:31:43', '2015-04-12 23:31:43');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_airlines_id_foreign` FOREIGN KEY (`airlines_id`) REFERENCES `airlines` (`id`),
  ADD CONSTRAINT `tickets_clients_id_foreign` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `transactions_clients_id_foreign` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `transactions_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
